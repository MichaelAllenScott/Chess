﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Model
{
    internal class Tile
    {
        #region Properties

        private bool _pieceOn;
        public bool PieceOn
        {
            get { return _pieceOn; }
            set { _pieceOn = value; }
        }

        private ChessColor.Color _color;
        public ChessColor.Color Color
        {
            get { return _color; }
            private set { _color = value; }
        }

        #endregion

    }
}
