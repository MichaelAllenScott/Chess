﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess.Model
{
    public abstract class Piece
    {
        #region Properties
        private ChessColor.Color _color;
        public ChessColor.Color Color
        {
            get { return _color; }
            private set { _color = value; }
        }

        

        #endregion
    }
}
